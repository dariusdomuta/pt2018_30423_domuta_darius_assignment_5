import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

public class Start {

    private static MonitoredData stringToObject(String string) {
        String[] tokens;
        tokens=string.split("\t\t");
        MonitoredData activity = new MonitoredData();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        activity.setStartTime(LocalDateTime.parse(tokens[0], formatter));
        activity.setEndTime(LocalDateTime.parse(tokens[1], formatter));
        activity.setActivityLabel(tokens[2]);
        return activity;
    }

    private static List<MonitoredData> getData() {
        String fileName = "C:\\Users\\dariu\\IdeaProjects\\Homework5\\src\\main\\resources\\activities.txt";
        List<MonitoredData> activityList = new ArrayList<MonitoredData>();
        MonitoredData activity = new MonitoredData();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(line -> activityList.add(stringToObject(line)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return activityList;
    }

    private static int daysCount(List<MonitoredData> activities) {
        return activities.stream().collect(
                groupingBy(a -> a.getStartTime().getDayOfYear())).size();
    }

    private static Map<String, Long> activityCount(List<MonitoredData> activities) {
        Map<String, Long> activitiesMap = new HashMap<String, Long>();
        List<String> activityLabels = activities.stream()
                .map(MonitoredData::getActivityLabel)
                .collect(Collectors.toList());
        activitiesMap = activityLabels.stream().collect(groupingBy(Function.identity(), Collectors.counting()));
        return activitiesMap;
    }

    private static Map<Integer, Map<String, Long>> activityPerDayCount(List<MonitoredData> activities) {
        Map<Integer, Map<String, Long>> activitiesPerDayCount = activities.stream().collect(
                (Collectors.groupingBy(a -> a.getStartTime().getDayOfYear(), (Collectors.groupingBy(MonitoredData::getActivityLabel, (Collectors.counting()))))));
        return activitiesPerDayCount;
    }

    private static void printMonitoredData(BufferedWriter bw) throws IOException {
        List<MonitoredData> activityList = getData();

        for(MonitoredData monitoredData : activityList) {
            bw.write(monitoredData.getStartTime() + "       " + monitoredData.getEndTime() + "      " + monitoredData.getActivityLabel() + "\n");
        }
    }

    private static void printNoMonitoredDays(BufferedWriter bw) throws IOException {
        List<MonitoredData> activityList = getData();
        bw.write(Integer.toString(daysCount(activityList)));
    }

    private static void printActivityCount(BufferedWriter bw) throws IOException {
        List<MonitoredData> activityList = getData();
        Map<String, Long> activityCountMap = activityCount(activityList);

        for(String name: activityCountMap.keySet()) {
            bw.write(name + " " + activityCountMap.get(name) + "\n");
        }
    }

    private static void printActivityPerDayCount(BufferedWriter bw) throws IOException {
        List<MonitoredData> activityList = getData();

        Map<Integer, Map<String, Long>> activityPerDayCountMap = activityPerDayCount(activityList);
        for(Integer day : activityPerDayCountMap.keySet()) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, 2011);
            c.set(Calendar.MONTH, Calendar.JANUARY);
            c.set(Calendar.DATE, 1);
            c.set(Calendar.HOUR, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.add(Calendar.DATE, day);
            Date date = c.getTime();
            bw.write(date.toString() + "\n");
            for (String name: activityPerDayCountMap.get(day).keySet()) {

                bw.write(name + "        " + activityPerDayCountMap.get(day).get(name) + "\n");
            }
            bw.write("\n");
        }

    }



    public static void main(String[] args) throws IOException {

        BufferedWriter bw = null;
        FileWriter fw = null;
        fw = new FileWriter("C:\\Users\\dariu\\IdeaProjects\\Homework5\\src\\main\\resources\\MonitoredData.txt");
        bw = new BufferedWriter(fw);




        System.out.println("Choose an opperation :\n 1.Print Monitored Data \n 2.Print the number of monitored days \n" +
                "3.Print the number of times each activity appears in monitoring time \n" + "4.Print the number of times" +
                " each activity appears daily \n");




        try {

                Scanner keyboard = new Scanner(System.in);
                int option = keyboard.nextInt();

                switch (option){
                    case 1: printMonitoredData(bw);
                        break;
                    case 2: printNoMonitoredDays(bw);
                        break;
                    case 3: printActivityCount(bw);
                        break;
                    case 4: printActivityPerDayCount(bw);
                        break;
                }


            //printMonitoredData(bw);
            //printNoMonitoredDays(bw);
            //printActivityCount(bw);
            //printActivityPerDayCount(bw);
        }catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
    }
}

